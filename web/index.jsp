<%-- 
    Document   : index
    Created on : 2013-12-14, 0:28:30
    Author     : Aphasia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link rel="stylesheet" type="text/css" href="css/my.css">
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>index Page</title>
    </head>
    <body background="imges/bg.jpg">
        <table width="80%"  border="1" align="center">
            <tr>
                <td height="94"colspan="2" align="center">
                    <!--引入head.jsp-->
                    <jsp:include flush="true" page="head.jsp"></jsp:include>
                    </td>
                </tr>
                <tr>
                    <td width="15%" height="185" align="center">
                    <jsp:include flush="true" page="left.jsp"></jsp:include>
                    </td>
                    <td width="73" align="center">
                    <jsp:include flush="true" page="right.jsp"></jsp:include>
                    </td>
                </tr>
                <tr>
                    <td height="62" colspan="2" align="center">
                    <jsp:include flush="true" page="tail.jsp"></jsp:include>
                </td>
            </tr>
        </table>
    </body>
</html>
