/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.zm.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Aphasia
 */
//这是一个model类，完成得到一个数据库连接的
public class ConnDB {
     private Connection conn= null;
    
    public Connection getConn(){
        try{
            Class.forName( "com.mysql.jdbc.Driver" ) ;
	    String sURL="jdbc:mysql://127.0.0.1:3306/shopping";
	    String sUserName="root";
	    String sPwd="";
            conn = DriverManager.getConnection(sURL,sUserName,sPwd);
	    Statement stmt = conn.createStatement();
            ResultSet rs = stmt.getResultSet(); // get any ResultSet that came from our query
        } catch(Exception e){
            //一定写上，不写错了不知道怎么调试
            e.printStackTrace();
        
        }
        return conn;
    
    }
}
