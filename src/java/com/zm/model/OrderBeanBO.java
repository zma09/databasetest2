/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//这是对orders.orserDetal的处理
package com.zm.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.apache.tomcat.jni.Time;

/**
 *
 * @author Aphasia
 */
public class OrderBeanBO {
     private PreparedStatement ps = null;
	private ResultSet rs = null;
	private Connection conn = null;
        
      
    public OrderInfoBean addOrder(MyCartBO mbo,String userId){
        OrderInfoBean oib = new OrderInfoBean();
        boolean b=true;
        try {
            conn=new ConnDB().getConn();
            ps=conn.prepareStatement("insert into orders (userId,isPayed,totalPrice) values(?,?,?)");
            ps.setString(1, userId);
            ps.setByte(2,(byte)0);
            ps.setFloat(3, mbo.getAllPrice());
            //执行
            int a=ps.executeUpdate();
            if (a==1) {
                //必须取出刚刚添加到orders表的那个订单号
                //取出最后的那条orders的id就是
                ps=conn.prepareStatement("select max(ordersId) from orders");
                rs=ps.executeQuery();
                int orderId=0;
                if(rs.next()){
                    orderId=rs.getInt(1);
                }
                //order表添加成功
                //添加orders细节表（orderDetail）表
                //从购物车中取出选购的商品
                ArrayList al= mbo.showMyCart();
                //循环的添加到orderDetail表
                //可以使用批量执行的方法提高操作数据库的效率
                Statement sm=conn.createStatement();
                        for(int i=0;i<al.size();i++){
                            GoodsBean gb=(GoodsBean)al.get(i);
                            sm.addBatch("insert into orderDetail values('"+orderId+"','"+gb.getGoodsId()+"','"+mbo.getGoodsNumById(gb.getGoodsId()+"")+"')");
                        }
                        //批量执行添加任务
                        sm.executeBatch();
                        String sql="select ordersId ,firstname,lastname,address,phone,totalPrice,username,email from users,orders"+
				" where ordersId=? and users.userid =(select orders.uesrid from orders where ordersId=?)";
                        
                        
                       ps=conn.prepareStatement(sql);
				ps.setInt(1, orderId);
				ps.setInt(2, orderId);
				rs=ps.executeQuery();
				if(rs.next()){
					//将re封装到OrderInfoBean
					oib.setOrdersId(rs.getInt(1));
					oib.setFirstname(rs.getString(2));
                                        oib.setLastname(rs.getString(3));
					oib.setAddress(rs.getString(4));
					oib.setPhone(rs.getString(5));
					oib.setTotalPrice(rs.getFloat(6));
					oib.setUsername(rs.getString(7));
					oib.setEmail(rs.getString(8));
                                }
            }
        } catch (Exception e) {
            b=false;
            e.printStackTrace();
        }finally{
            //关闭资源
            this.close();
        }
        if(b){
			return oib;
		}else{
			return null;
		}
    }
    public void close() { // 关闭各种打开的资源
		try {
			if (rs != null) {
				rs.close();
				rs = null;
			}
			if (ps != null) {
				ps.close();
				ps = null;
			}
			if (conn != null) {
				conn.close();
				conn = null;
			}
		} catch (Exception e) {
			e.printStackTrace();// 打印异常，以便修改
		}
	}
}
