/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//这是一个model，表示数users表
package com.zm.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Aphasia
 */
public class UserBeanBO {
      private PreparedStatement ps = null;
	private ResultSet rs = null;
	private Connection conn = null;
        //根据用户返回该用户的全部信息
        public UserBean getUserBean(String u){
            UserBean ub =new UserBean();
            try {
                 conn=new ConnDB().getConn();
                 ps = conn.prepareStatement("select * from users where username = ?");
                 ps.setString(1, u);
                 rs=ps.executeQuery();
                 if(rs.next()){
                     ub.setUserid(rs.getInt(1));
                     ub.setUsername(rs.getString(2));
                     ub.setFirstname(rs.getString(3));
                     ub.setLastname(rs.getString(4));
                     ub.setPasswd(rs.getString(5));
                     ub.setEmail(rs.getString(6));
                     ub.setPhone(rs.getString(7));
                     ub.setAddress(rs.getString(8));
                     ub.setGrade(rs.getInt(9));
                 }
            } catch (Exception e) {
            }finally{
            this.close();
            }
            return ub;
        }
        
   public boolean checkUser(String u,String p){
       boolean b=false;
       
       try {
            conn=new ConnDB().getConn();
            ps = conn.prepareStatement("select passwd from users where username = ?");
            //共有多少条记录
            ps.setString(1, u);
            rs=ps.executeQuery();
            
            if (rs.next()) {
               //取出数据库密码
                String dbPasswd = rs.getString(1);
                
                if(dbPasswd.equals(p)){}
                b=true;
            }
       } catch (Exception e) {
          e.printStackTrace();
       }finally{
         this.close();
       }
       return b;
   }
   public void close() { // 关闭各种打开的资源
		try {
			if (rs != null) {
				rs.close();
				rs = null;
			}
			if (ps != null) {
				ps.close();
				ps = null;
			}
			if (conn != null) {
				conn.close();
				conn = null;
			}
		} catch (Exception e) {
			e.printStackTrace();// 打印异常，以便修改
		}
	}
}
