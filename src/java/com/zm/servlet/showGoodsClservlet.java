/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zm.servlet;

import com.zm.model.GoodsBeanBO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.zm.model.*;

/**
 *
 * @author Aphasia
 */
public class showGoodsClservlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //PrintWriter out = response.getWriter();
        //processRequest(request, response);
        //得到type判断用户是分页还是像素货物的详细信息
        String type = request.getParameter("type");

        if (type.equals("showDetail")) {

            //得到要显示的货物ID
            String goodsId = request.getParameter("id");
            //调用GoodsBeanBO()[可有得到商品货物的具体信息]
            GoodsBeanBO gbb = new GoodsBeanBO();
            GoodsBean gb = gbb.getGoodsBean(goodsId);
            //把gb放入request
            request.setAttribute("goodsInfo", gb);
            //跳转
            request.getRequestDispatcher("showDetail.jsp").forward(request, response);
        } else if (type.equals("fenye")) {
            //得到pageNow
            System.out.println("123");
            String pageNow = request.getParameter("pageNow");

            //把pageNow放入到request
            request.setAttribute("pageNowNowNow", pageNow);
            //跳回购物大厅
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }

}
