/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.zm.servlet;

import com.zm.model.UserBean;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.zm.model.*;
/**
 *
 * @author Aphasia
 */
public class ShoppingCl2 extends HttpServlet {

  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        PrintWriter out = response.getWriter();
        //这个控制器，用于检验检测用户是否登录过，如果等率就进入登录界面
        //如果登录过，就直接显示用户信息和购物车页面
        //看看session中是否有用户登录的信息
        
        UserBean ub=(UserBean)request.getSession().getAttribute("userInfo");
        if(ub==null){
            //说明用户没有登录过
            //就跳转到shopping.jsp
            request.getRequestDispatcher("shopping2.jsp").forward(request, response);
        }else{
            //说明登录过
            //就跳转到shopping3.jsp
            request.getRequestDispatcher("shopping3.jsp").forward(request, response);
        }
    }

   
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        this.doGet(request, response);
    }


}
