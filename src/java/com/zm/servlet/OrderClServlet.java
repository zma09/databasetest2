/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.zm.servlet;

import com.zm.model.OrderBeanBO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.zm.model.*;
/**
 *
 * @author Aphasia
 */
public class OrderClServlet extends HttpServlet {

 
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        //该任务将会交给OrderBeanBO 处理完成订单的任务（把订单写入数据库）
        OrderBeanBO obb=new OrderBeanBO();
        //得到购物车
        MyCartBO mcb=(MyCartBO)request.getSession().getAttribute("mycart");
        //用户的Id
        int userId=((UserBean)request.getSession().getAttribute("userInfo")).getUserid();
        OrderInfoBean oib=obb.addOrder(mcb,userId+"");
        if(oib!=null){
        //添加成功
            //准备数据显示订单的详细信息的数据给下一个页面shopping4.jsp
            request.setAttribute("detailbean", oib);
            request.getRequestDispatcher("shopping4.jsp").forward(request, response);
        }else{
            //添加订单失败
            request.getRequestDispatcher("shopping3.jsp").forward(request, response);
        }
        
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        this.doGet(request, response);
    }

 
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
